<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div class="container">
 *
 * @package SKT Simple
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
</head>
<body <?php body_class(''); ?>>
<div id="main">
<div class="headerarea">
  <div class="container">
    <div class="header-top">
      <div class="header-left">
        <div class="column1"><i class="fa fa-user"></i><?php if( '' !== get_theme_mod('header_phone') ) { echo esc_attr(get_theme_mod('header_phone', __('+11 123 456 7890', 'skt-simple'))); } ?></div>
        <!--column1-->
        <div class="column1"><span><?php if( '' !== get_theme_mod('opning_hours')) { echo wp_kses_post(get_theme_mod('opning_hours', __('Mon. - Fri. 10:00 - 21:00', 'skt-simple'))); } ?></span></div>
        <!--column1--> 
      </div>
      <div class="header-right">
        <div class="social-icons">
              <?php if ( '' !== get_theme_mod( 'fb_link' ) ) { ?>
              <a title="facebook" class="fa fa-facebook fa-1x" target="_blank" href="<?php echo esc_url(get_theme_mod('fb_link','http://www.facebook.com')); ?>"></a>
              <?php } ?>
              <?php if ( '' !== get_theme_mod( 'twitt_link' ) ) { ?>
              <a title="twitter" class="fa fa-twitter fa-1x" target="_blank" href="<?php echo esc_url(get_theme_mod('twitt_link','https://twitter.com')); ?>"></a>
              <?php } ?>
              <?php if ( '' !== get_theme_mod('gplus_link') ) { ?>
              <a title="google-plus" class="fa fa-google-plus fa-1x" target="_blank" href="<?php echo esc_url(get_theme_mod('gplus_link','https://plus.google.com')); ?>"></a>
              <?php }?>
              <?php if ( '' !== get_theme_mod('linked_link') ) { ?>
              <a title="linkedin" class="fa fa-linkedin fa-1x" target="_blank" href="<?php echo esc_url(get_theme_mod('linked_link','http://linkedin.com')); ?>"></a>
              <?php } ?>
            </div>
        <!--column2--> 
      </div>
      <div class="clear"></div>
    </div>
    <!-- header-top -->
    <div class="header-inner <?php if( get_theme_mod( 'hide_slides' ) != '' || !is_front_page() && !is_home()) { ?>rel<?php } ?>">
      <!-- logo -->
      <div class="logo">
			<?php skt_simple_the_custom_logo(); ?>
            <a href="<?php echo esc_url(home_url('/')); ?>"><h1><?php bloginfo('name'); ?></h1></a>
            <span class="tagline"><?php bloginfo('description'); ?></span>
            </div>
      <!-- logo -->
	  <div class="toggle">
         <a class="toggleMenu" href="<?php echo esc_url('#');?>"><?php _e('Menu','skt-simple'); ?></a>
     </div><!-- toggle --> 
      <div class="sitenav">
          <?php wp_nav_menu(array('theme_location' => 'primary')); ?>
      </div><!-- site-nav -->  
      
      <div class="clear"></div>
    </div>
    <!-- header-inner -->
<?php if ( is_front_page()) { ?>    
    <!-- Slider Section -->
<?php for($sld=7; $sld<10; $sld++) { ?>
<?php if( get_theme_mod('page-setting'.$sld)) { ?>
<?php $slidequery = new WP_query('page_id='.get_theme_mod('page-setting'.$sld,true)); ?>
<?php while( $slidequery->have_posts() ) : $slidequery->the_post();
$image = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
$img_arr[] = $image;
$id_arr[] = $post->ID;
endwhile;
}
}
?>
<?php if(!empty($id_arr)){ ?>
<section id="home_slider" <?php if( get_theme_mod( 'hide_slides' ) != '') { ?> class="none"<?php } ?>>
<div class="slider-wrapper theme-default">
<div id="slider" class="nivoSlider">
	<?php 
	$i=1;
	foreach($img_arr as $url){ ?>
    <img src="<?php echo esc_url($url); ?>" title="#slidecaption<?php echo $i; ?>" />
    <?php $i++; }  ?>
</div>   
<?php 
$i=1;
foreach($id_arr as $id){ 
$title = get_the_title( $id ); 
$post = get_post($id); 
$content = apply_filters('the_content', substr(strip_tags($post->post_content), 0, 150)); 
?>                 
<div id="slidecaption<?php echo $i; ?>" class="nivo-html-caption">
<div class="slide_info">
<h2><?php echo $title; ?></h2>
<?php echo $content; ?>
<a class="sldbutton" href="<?php the_permalink(); ?>"><?php esc_attr_e('Read More', 'skt-simple');?></a>
</div>
</div>      
<?php $i++; } ?>       
 </div>
<div class="clear"></div>        
</section>
<?php } else { ?>
<section id="home_slider" <?php if( get_theme_mod( 'hide_slides' ) != '') { ?> class="none"<?php } ?>>
<div class="slider-wrapper theme-default">
    <div id="slider" class="nivoSlider">
        <img src="<?php echo esc_url( get_template_directory_uri() ) ; ?>/images/slides/slider1.jpg" alt="" title="#slidecaption1" />
        <img src="<?php echo esc_url( get_template_directory_uri() ) ; ?>/images/slides/slider2.jpg" alt="" title="#slidecaption2" />
        <img src="<?php echo esc_url( get_template_directory_uri() ) ; ?>/images/slides/slider3.jpg" alt="" title="#slidecaption3" />
    </div>                    
      <div id="slidecaption1" class="nivo-html-caption">
        <div class="slide_info">
                <h2><?php esc_attr_e('Welcome To SKT Simple','skt-simple'); ?></h2>
                <p><?php esc_attr_e('Phasellus nec metus scelerisque, feugiat est quis, vestibulum ante. Proin id vehicula enim.','skt-simple'); ?></p>
        </div>
        <a href="<?php echo esc_url('#');?>" class="sldbutton"><?php esc_attr_e('Read More','skt-simple');?></a>
        </div>
        
        <div id="slidecaption2" class="nivo-html-caption">
            <div class="slide_info">
                    <h2><?php esc_attr_e('Truly Wordpress Theme','skt-simple'); ?></h2>
                        <p><?php esc_attr_e('Phasellus nec metus feugiat est quis, vestibulum ante. Proin id vehicula enim feugiat est quis, vestibulum ante habitant','skt-simple'); ?></p></div>
            <a href="<?php echo esc_url('#');?>" class="sldbutton"><?php esc_attr_e('Read More','skt-simple');?></a>
        </div>
        
        <div id="slidecaption3" class="nivo-html-caption">
            <div class="slide_info">
                    <h2><?php esc_attr_e('Best Wordpress Themes','skt-simple'); ?></h2>
                        <p><?php esc_attr_e('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla commodo enim nisl, eget fringilla arcu feugiat Pellentesque.','skt-simple'); ?></p></div>
            <a href="<?php echo esc_url('#');?>" class="sldbutton"><?php esc_attr_e('Read More','skt-simple');?></a>
        </div>
</div>
<div class="clear"></div>
</section>
<?php } ?>

<!-- Other Space-->
<?php } ?>
<!-- Slider Section -->
    <!-- slider -->    
  </div>
  <!-- Container --> 
</div>
<div class="clear"></div>
<?php if ( is_front_page()) { ?>   
<div class="container">
<div class="boxes_content">
<?php if( get_theme_mod( 'hide_column' ) == '') { ?>   
<div id="wrapOne">
      <div class="one_four_page-wrap">
            <?php for($fx=1; $fx<5; $fx++) { ?>
        	<?php if( get_theme_mod('page-column'.$fx,false) ) { ?>
        	<?php $queryvar = new wp_query('page_id='.get_theme_mod('page-column'.$fx,true));				
			while( $queryvar->have_posts() ) : $queryvar->the_post(); ?> 
            <div class="one_four_page <?php if($fx % 4 == 0) { echo "last_column"; } ?>">
                <div class="one_four_thumb">
				  <?php if ( has_post_thumbnail() ) { ?>
                  	<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail();?></a>
				  <?php } ?>
                   </div>
                <div class="one_four_page_content">
                <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                <?php the_excerpt(); ?>
                <a class="read-more" href="<?php the_permalink(); ?>"><?php esc_attr_e('Read More','skt-simple');?></a>
                </div>
            </div>
             <?php endwhile;
						wp_reset_query(); ?>
        <?php } else { ?>
        <div class="one_four_page <?php if($fx % 4 == 0) { echo "last_column"; } ?>">
                <div class="one_four_thumb"><img src="<?php echo esc_url( get_template_directory_uri()); ?>/images/special-icon<?php echo $fx; ?>.png"></div>
                <div class="one_four_page_content">
                <h4><a href="<?php echo esc_url('#');?>"><?php echo esc_attr('Multi Purpose '.$fx, 'skt-simple'); ?></a></h4>
                <p>
                <?php esc_attr_e('Phasellus nec metus scelerisque, feugiat est quis, vestibulum ante. Proin id vehicula enim. Pellentesque habitant.','skt-simple');?>
                </p>
                <a class="read-more" href="<?php echo esc_url('#');?>"><?php esc_attr_e('Read More','skt-simple');?></a>
                </div>
            </div>
        <?php } }?>
         </div>
        <!-- .one_four_page-wrap-->
        <div class="clear"></div>
    </div>
<?php } ?>    
    </div>
    
<?php if( get_theme_mod( 'hide_about' ) == '') { ?>    
<section class="sectionbg">
  <div class="about-section">
	<?php if( get_theme_mod('page-setting1')) { ?>
    <?php $queryvar = new WP_query('page_id='.get_theme_mod('page-setting1' ,true)); ?>
    <?php while( $queryvar->have_posts() ) : $queryvar->the_post();?> 	
    <h2 class="section_title"><?php the_title(); ?></h2>
    <p><?php the_post_thumbnail( array(492,399, true));?></p>
    <div class="about-services"><?php the_content(); ?></div>
    <?php endwhile; } else { ?> 
    <h2 class="section_title"><?php esc_attr_e('About SKT','skt-simple');?> <span><?php esc_attr_e('SIMPLE','skt-simple');?></span></h2>
    <p><img src="<?php echo esc_url( get_template_directory_uri()); ?>/images/about-left.jpg"></p>
    <div class="about-services">
      <div class="services-col"> <i class="fa fa-check"></i>
        <div class="services-col-content">
          <h4><?php esc_attr_e('Expert Team','skt-simple');?></h4>
          <p><?php esc_attr_e('Nulla et ullamcorper sapien. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum ac justo ornare, blan posuere lorem. Donec tempus libero euismod posuere sagittis...','skt-simple');?></p>
        </div>
      </div>
      <div class="services-col"> <i class="fa fa-check"></i>
        <div class="services-col-content">
          <h4><?php esc_attr_e('Professional Company','skt-simple');?></h4>
          <p><?php esc_attr_e('Nulla et ullamcorper sapien. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum ac justo ornare, blan posuere lorem. Donec tempus libero euismod posuere sagittis...','skt-simple');?></p>
        </div>
      </div>
      <div class="services-col"> <i class="fa fa-check"></i>
        <div class="services-col-content">
          <h4><?php esc_attr_e('Best Services','skt-simple');?></h4>
          <p><?php esc_attr_e('Nulla et ullamcorper sapien. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum ac justo ornare, blan posuere lorem. Donec tempus libero euismod posuere sagittis...','skt-simple');?></p>
        </div>
      </div>
    </div>
	<?php } ?>
  </div>
  <!-- middle-align -->
  <div class="clear"></div>
</section>
<?php } ?>    
    
    </div>        
<?php } ?>    