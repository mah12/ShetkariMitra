<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #main div and all content after
 *
 * @package SKT Simple
 */
?>
<div id="footer-wrapper">
  <div class="container">
    <div class="footer">
      <div class="cols-3">
        <div class="widget-column-1">
          <h3><?php echo esc_html(get_theme_mod('about_title',__('About SKT Simple','skt-simple'))); ?></h3>
          <p><?php echo wp_kses_post(get_theme_mod('about_description',__('Nam aliquet aliquam ipsum eget volutpat. Duis nec porta purus. Integer adipiscing augue sit amet libero vulputate, et fermentum nibh rutrum. In bibendum nisi sed consequat hendrerit. Aliquam. <br /> </br > Quisque elementum, dolor nec tempus eleifend, nibh mauris aliquet ante, eu tempus sapien nisi non nunc. Nulla facilisi. Suspendisse lobortis laoreet risus, a posuere mauris facilisis at. In viverra euismod velit non cursus.','skt-simple'))); ?></p>
        </div>
        <div class="widget-column-2">
          <h3><?php echo esc_html(get_theme_mod('recent_title',__('Recent Posts','skt-simple'))); ?></h3>
          <?php $args = array( 'posts_per_page' => 2, 'post__not_in' => get_option('sticky_posts'), 'orderby' => 'date', 'order' => 'desc' ); $the_query = new WP_Query( $args ); ?>
      <?php while ( $the_query->have_posts() ) :  $the_query->the_post(); ?>
          <div class="recent-post"> <a href="<?php the_permalink(); ?>">
        <?php if ( has_post_thumbnail() ) { $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );   $thumbnailSrc = $src[0]; ?>
        <img src="<?php echo esc_url($thumbnailSrc); ?>" width="60" height="auto" ><?php } else { ?>
        <img src="<?php echo esc_url( get_template_directory_uri()); ?>/images/post-thumb-small-150x150.jpg" width="60"  />
        <?php } ?></a>
            <h5><?php the_title(); ?></h5>
            <span class="footer-post-author"><?php the_author(); ?></span><span class="footer-post-date"><?php echo get_the_date(); ?></span>
            <div class="clear"></div>
          </div>
          <?php endwhile; ?>
        </div>
        <div class="widget-column-3">
          <h3><?php echo esc_html(get_theme_mod('contact_title',__('Contact Info','skt-simple'))); ?></h3>
          <p class="parastyle"><?php echo esc_html(get_theme_mod('contact_add',__('Etiam luctus venenatis magna, at facilisis leo sagittis in.','skt-simple'))); ?></p>
          <div class="phone-no">
          	<?php if( '' !== get_theme_mod('header_phone') ) { ?>
            <p><span class="h1firstword"><?php esc_attr_e('Phone:','skt-simple');?></span><?php echo esc_attr(get_theme_mod('header_phone', __('+11 123 456 7890', 'skt-simple')));?></p>
            <?php } ?>
            <?php if('' !== get_theme_mod('footer_email')){?>
            <p><span class="h1firstword"><?php esc_attr_e('Email:','skt-simple');?></span><a href="mailto:<?php echo sanitize_email(get_theme_mod('footer_email','info@sitename.com', 'skt-simple')); ?>"><?php echo esc_attr(get_theme_mod('footer_email','info@sitename.com', 'skt-simple')); ?></a></p>
            <?php } ?>
            <?php if('' !== get_theme_mod('footer_website')){?>
            <p><span class="h1firstword"><?php esc_attr_e('Website:','skt-simple');?></span><?php echo esc_attr(get_theme_mod('footer_website','http://sitename.com', 'skt-simple'))?></p>
            <?php } ?>
          </div>
          <div class="social-icons">
          <?php if ( '' !== get_theme_mod( 'fb_link' ) ) { ?>
          <a title="facebook" class="fa fa-facebook" target="_blank" href="<?php echo esc_url(get_theme_mod('fb_link','http://www.facebook.com')); ?>"></a> 
          <?php } ?>
          <?php if ( '' !== get_theme_mod( 'twitt_link' ) ) { ?>
          <a title="twitter" class="fa fa-twitter" target="_blank" href="<?php echo esc_url(get_theme_mod('twitt_link','https://twitter.com')); ?>"></a>
          <?php } ?>
          <?php if ( '' !== get_theme_mod('gplus_link') ) { ?>
          <a title="google-plus" class="fa fa-google-plus" target="_blank" href="<?php echo esc_url(get_theme_mod('gplus_link','https://plus.google.com')); ?>"></a>
          <?php } ?>
          <?php if ( '' !== get_theme_mod('linked_link') ) { ?>
          <a title="linkedin" class="fa fa-linkedin" target="_blank" href="<?php echo esc_url(get_theme_mod('linked_link','http://linkedin.com')); ?>"></a>
          <?php } ?>
          </div>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
      </div>
      <!--end .cols-3--> 
    </div>
    <!--end .footer-->
    <div class="copyright">
      <div class="copyright-txt"><?php esc_attr_e('&copy; 2016','skt-simple');?> <?php bloginfo('name'); ?>. <?php esc_attr_e('All Rights Reserved', 'skt-simple');?></div>
      <div class="design-by"><?php printf('<a href="'.esc_url(SKT_FREE_THEME_URL).'" rel="nofollow" target="_blank">SKT Simple Theme</a>' ); ?></div>
      <div class="clear"></div>
    </div>
    <!-- copyright --> 
  </div>
  <!--end .container--> 
</div>
</div> 
<?php wp_footer(); ?>
</body></html>