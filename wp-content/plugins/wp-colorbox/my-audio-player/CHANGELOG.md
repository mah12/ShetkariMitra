### 1.0 / 27-03-2018 - 09-04-2018###

**Development**

* Created design elements (buttons, gradients, etc)
* Created HTML5 structure/content
* Styled HTML using CSS
* Created initializer function to load tracks and info
* Play functionality using audio.play
* Pause functionality using audio.pause
* Duration functionality
* Change track next and prev
