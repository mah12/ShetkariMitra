<?php 

/*
* @package myaudioplugin
*/

/*
Plugin Name: myaudioplugin
Plugin URI: http://www.coep.org.in
Description: This is my first plugin
Version: 1.0.0
Author: Shubham Devkate
Author URI: http://www.coep.org.in
License: GPLv2 or later
Text Domain: myaudioplugin
*/

/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.
*/

// if outsider want to interact to wp, then kill the process.
/*
if (! defined('ABSPATH')) {
	die;
}

defined('ABSPATH') or die('You cannot access this file');

*/
if (! function_exists('add_action')) {
	echo "You cannot access the file.";
	exit;
}

// create php class
class myaudioplugin 
{

	function __construct(){
		add_action('init', array($this, 'custom_post_type'));
	}

	function register(){
		add_action('wp_enqueue_scripts', array($this, 'enqueue'));
	}
	// methods
	function activate(){
		//remember to call custom_post_type books & save the data in wp_posts
		$this -> custom_post_type();
		//wp refresh and check everything created new
		flush_rewrite_rules();
	}

	function deactivate(){
		//flush rewrite rules
		flush_rewrite_rules();
	}

	// function uninstall(){

	// }

	function custom_post_type(){
		register_post_type('book', ['public' => true, 'label'=> 'Books']);
	}

	function enqueue(){
		// enqueue the scripts
		wp_enqueue_style('mypluginstyle', plugins_url('/assets/mystyle.css', __FILE__));
	}
}

//initialize class
if (class_exists('myaudioplugin')) {
	$myaudioplugin = new myaudioplugin();
	$myaudioplugin->register();
}

// activation
register_activation_hook(__FILE__, array($myaudioplugin, 'activate'));


// deactivation
register_deactivation_hook(__FILE__, array($myaudioplugin, 'deactivate'));


// Refer uninstall.php for more information.
// unistall  
// limitation : Only a static class method or function can be used in an uninstall hook
// register_uninstall_hook(__FILE__, array($myaudioplugin, 'uninstall'));
 