<?php  

/* 
* Trigger this file on plugin uninstall
*
*@package myaudioplugin
*/ 

// security check for uninstall.php
if (! defined('WP_UNINSTALL_PLUGIN')) {
	die;
}

// clear database stored data
// $books = get_posts(array('post_type' => 'book', 'numberposts' => -1));

// // Delete the posts from database
// foreach($books as $book){
// 	wp_delete_post($book->ID, true);
// }

// trigger sql query directly into database
global $wpdb;
$wpdb->query( "DELETE FROM wp_posts WHERE post_type = 'book'" );
$wpdb->query("DELETE FROM wp_postmeta WHERE post_id NOT IN(SELECT id FROM wp_posts)");
$wpdb->query("DELETE FROM wp_term_relationships WHERE post_id NOT IN(SELECT id FROM wp_posts)");