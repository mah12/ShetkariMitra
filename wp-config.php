<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'shubham');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'S+{%S[iKu9{.=cX,W=&sl)).>oHDe=6|28*>`C0An/*Q+@<P_3@NP#v*+/8kFnFe');
define('SECURE_AUTH_KEY',  '+0>a<l.U0TGY$s6cEBAyw-:vO XGK&M)p|ip2[l:(10 PR!sW?;CM+uO./De,R}O');
define('LOGGED_IN_KEY',    '2YJ*4!y39C4yTI1B<MT(Fq@Lo8$-Ol<(T#3[KWGjVg^5?.>&<epCIJv7.jtRHM:j');
define('NONCE_KEY',        'QQO2Iis`;I,wCJ;3Aygw@Qc-u~Y0h%`+{T.]YtN$ b}I8*GoC7wNoX,SA6]:XTYP');
define('AUTH_SALT',        'pKCGe2H(znmE8O^(#ApwMZQma[V,nr|es 9uj4B2|bV*Qtv751qc?2x,EbOjE-GG');
define('SECURE_AUTH_SALT', 'PtO~r{Fd39G5^^KBd@%Ec*/>I.1?<^+5qn<5l9eVWP3(sBz(LSN-bTRAt68UV:#R');
define('LOGGED_IN_SALT',   '3i5!9[5z+Wx)2[fiv])(?Y7{oe# rqhgUW*q@b7iK|E[{w`82]KL~Trmjita(4Q%');
define('NONCE_SALT',       '4{JT+!U[I`VT~j_.:>=3dPAR4U4):h=W$%GJB:4N(e*LY*:I&kxiZ`<O**kXv__|');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'all_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/** FS_METHOD forces the filesystem method. It should only be "direct", "ssh2", "ftpext", or "ftpsockets". Generally, you should only change this if you are experiencing update problems. If you change it and it doesn't help, change it back/remove it. Under most circumstances, setting it to 'ftpsockets' will work if the automatically chosen method does not. Note that your selection here has serious security implications. If you are not familiar with them, you should seek help before making a change. */
 define('FS_METHOD', 'direct');
